module.exports.responseStatus = (statusCode, body) => {
    return {
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Headers": "Access-Control-Allow-Credentials, Access-Control-Allow-Origin,Content-Type,Authorization"
        },
        statusCode: statusCode,
        body: JSON.stringify(body)
    }
}